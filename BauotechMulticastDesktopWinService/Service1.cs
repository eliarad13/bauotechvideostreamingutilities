﻿using BauotechPlayerCSLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace BauotechMulticastDesktop
{
    public partial class Service1 : ServiceBase
    {
        BauotechPlayer m_player = new BauotechPlayer();
        public Service1()
        {
            InitializeComponent();
        }
        public void Debug()
        {
            OnStart(null);
        }

        protected override void OnStart(string[] args)
        {

            File.WriteAllText("c:\\bm.txt", "OnStart");

            int r;
            if ((r = m_player.InitializeScreenCaptureToRemoteClient("234.5.5.5", "",
                                                               6000,
                                                               true,
                                                               true,
                                                               true,
                                                               1316,
                                                               true,
                                                               true)) < 0)
            {
                File.WriteAllText("c:\\bm.txt", "InitializeScreenCaptureToRemoteClient: " + r);
            }
            else
            {
                m_player.Run();
                File.WriteAllText("c:\\bm.txt", "started");
            }
             

        }

        protected override void OnStop()
        {
           
            if (m_player != null)
            {
                m_player.Stop();
                m_player.Close();
            }
            File.WriteAllText("c:\\bm.txt", "Closed");
            
        }
    }
}

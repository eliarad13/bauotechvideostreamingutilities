﻿using BauotechPlayerCSLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BauotechMulticastWinForm
{
    public partial class Form1 : Form
    {
        BauotechPlayer m_player = new BauotechPlayer();
        public Form1()
        {
            InitializeComponent();
            this.Size = new Size(0, 0);


            string[] args = Environment.GetCommandLineArgs();
             
            Task.Run(() =>
            {
                try
                {
                    string ipAddress = "234.5.5.5";
                    int port = 6000;
                    if (args.Length == 3)
                    {
                        ipAddress = args[1];
                        port = int.Parse(args[2]);
                    }
                    if (m_player.InitializeScreenCaptureToRemoteClient(ipAddress, "",
                                                                       port,
                                                                       true,
                                                                       true,
                                                                       true,
                                                                       1316,
                                                                       true,
                                                                       true) < 0)
                    {
                        MessageBox.Show("Failed to start");
                    }
                    else
                    {
                        m_player.Run();
                    }
                }
                catch (Exception err)
                {
                    MessageBox.Show(err.Message);
                }
            });
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_player.Stop();
            m_player.Close();
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            Hide();
        }
    }
}
